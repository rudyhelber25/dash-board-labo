import { TestBed } from '@angular/core/testing';

import { CustomFirstService } from './custom-first.service';

describe('CustomFirstService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CustomFirstService = TestBed.get(CustomFirstService);
    expect(service).toBeTruthy();
  });
});
