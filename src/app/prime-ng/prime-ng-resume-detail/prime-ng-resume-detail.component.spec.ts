import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimeNgResumeDetailComponent } from './prime-ng-resume-detail.component';

describe('PrimeNgResumeDetailComponent', () => {
  let component: PrimeNgResumeDetailComponent;
  let fixture: ComponentFixture<PrimeNgResumeDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrimeNgResumeDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimeNgResumeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
