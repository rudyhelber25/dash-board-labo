import { Component, OnInit } from '@angular/core';
import {PrimerNgDetailService} from "../prime-ng-detail/primer-ng-detail.service";
import {ActivatedRoute, ParamMap} from "@angular/router";

@Component({
  selector: 'app-prime-ng-resume-detail',
  templateUrl: './prime-ng-resume-detail.component.html',
  styleUrls: ['./prime-ng-resume-detail.component.scss']
})
export class PrimeNgResumeDetailComponent implements OnInit {

  public product: { id: number, name: string, description: string };

  constructor(private primerNgDetailService: PrimerNgDetailService,
              private  activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      let id = params.get('id');
      this.product = this.primerNgDetailService.getById(+id);
    });
  }

}
