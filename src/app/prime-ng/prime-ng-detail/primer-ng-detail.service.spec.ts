import { TestBed } from '@angular/core/testing';

import { PrimerNgDetailService } from './primer-ng-detail.service';

describe('PrimerNgDetailService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PrimerNgDetailService = TestBed.get(PrimerNgDetailService);
    expect(service).toBeTruthy();
  });
});
