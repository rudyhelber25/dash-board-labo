import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimeNgDetailComponent } from './prime-ng-detail.component';

describe('PrimeNgDetailComponent', () => {
  let component: PrimeNgDetailComponent;
  let fixture: ComponentFixture<PrimeNgDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrimeNgDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimeNgDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
