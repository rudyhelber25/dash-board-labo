import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PrimerNgDetailService {

  private products: { id: number, name: string, description: string }[];

  constructor() {
    this.products = [
      {
        id: 1,
        name: 'Coca cola',
        description: 'Gaseosa Sabor Cola.'
      },
      {
        id: 2,
        name: 'Fanta',
        description: 'Gaseosa Sabor Naranja.'
      },
      {
        id: 3,
        name: 'Sprite',
        description: 'Gaseosa Sin Sabor.'
      },
      {
        id: 4,
        name: 'Simba',
        description: 'Gaseosa MultiSabor.'
      }
    ];
  }

  public getById(id: number): { id: number, name: string, description: string } {
    let response: { id: number, name: string, description: string } = null;

    for (let product of this.products) {
      if (product.id === id) {
        response = product;
      }
    }
    return response;
  }

  get getProducts(): { id: number, name: string, description: string }[] {
    return this.products;
  }
}
