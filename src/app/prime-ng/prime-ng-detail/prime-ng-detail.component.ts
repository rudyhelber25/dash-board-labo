import { Component, OnInit } from '@angular/core';
import {PrimerNgDetailService} from "./primer-ng-detail.service";

@Component({
  selector: 'app-prime-ng-detail',
  templateUrl: './prime-ng-detail.component.html',
  styleUrls: ['./prime-ng-detail.component.scss']
})
export class PrimeNgDetailComponent implements OnInit {

  public products: { id: number, name: string, description: string }[];

  constructor(private primerNgDetailService: PrimerNgDetailService) {
    this.products = [];
  }

  ngOnInit() {
    this.products = this.primerNgDetailService.getProducts;
  }

}
