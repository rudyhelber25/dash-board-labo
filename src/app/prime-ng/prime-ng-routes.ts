import {Routes} from '@angular/router';
import {PrimeNgMainComponent} from "./prime-ng-main/prime-ng-main.component";
import {PrimeNgDataComponent} from "./prime-ng-data/prime-ng-data.component";
import {PrimeNgDetailComponent} from "./prime-ng-detail/prime-ng-detail.component";
import {PrimeNgResumeDetailComponent} from "./prime-ng-resume-detail/prime-ng-resume-detail.component";

export const PRIME_NG_ROUTES_CONFIG: Routes = [
  {
    path: '',
    component: PrimeNgMainComponent,
    children: [
      {
        path: '',
        redirectTo: '/prime-ng/data',
        pathMatch: 'full'
      },
      {
        path: 'data',
        component: PrimeNgDataComponent,
        data: {title: 'Laboratorio 3', content: 'this data was received from router and it is working!!!'}
      },
      {
        path: 'detail',
        component: PrimeNgDetailComponent,
        children: [
          {
            path: ':id',
            component: PrimeNgResumeDetailComponent,
          }
        ]
      }
    ]
  }
];
